backintime (1.5.3-1) UNRELEASED; urgency=medium

  * New upstream version 1.5.3 (Closes: #985257)
  * Add nocache to suggests of backintime-common (Closes: #1084024)
  * d/watch: correct sorting pre release versions (Closes: #1087686)
  * update d/copyright

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Sun, 17 Nov 2024 18:04:10 +0100

backintime (1.5.2-1) unstable; urgency=medium

  [ Fabio Fantoni ]
  * New upstream version 1.5.2
  * d/control: changes deps from qt5 to qt6
  * use dh-sequence-python3
  * Update standards version to 4.7.0, no changes needed
  * update d/copyright

  [ Jonathan Wiltshire ]
  * Promote sshfs to Recommends (Closes: #1072474)

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 10 Aug 2024 12:38:02 +0100

backintime (1.4.3-1) unstable; urgency=medium

  * New upstream version 1.4.3 (Closes: #990343)
  * d/patches/dbus-in-usr-share.patch: remove (applied upstream)
  * Update d/copyright

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Sun, 25 Feb 2024 13:30:41 +0000

backintime (1.4.1-1) unstable; urgency=medium

  [ Fabio Fantoni ]
  * New upstream release (Closes: #998105, #973760)
  * d/patches/release-1.3.3.patch: remove (applied upstream)
  * Update d/copyright

  [ Jonathan Wiltshire]
  * Add Fabio Fantoni to uploaders
  * backintime-qt: install dbus configuration file in /usr/share; remove copy
    in /etc if not modified

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 30 Dec 2023 18:07:55 +0000

backintime (1.3.3-4) unstable; urgency=medium

  [ Fabio Fantoni ]
  * Add python3-packaging to backintime-common depends.
    Thanks to Anabelle Vandenburgh (Closes: #1030051)
  * d/patches: add upstream commit "Release 1.3.3" that was missed in source tag
    (Closes: #1030506)
  * Update standards version to 4.6.2, no changes needed

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 04 Feb 2023 22:03:58 +0000

backintime (1.3.3-3) unstable; urgency=medium

  * Source upload, no changes

 -- Jonathan Wiltshire <jmw@debian.org>  Fri, 20 Jan 2023 08:58:12 +0000

backintime (1.3.3-2) unstable; urgency=medium

  * Add depends on pkexec

 -- Jonathan Wiltshire <jmw@debian.org>  Thu, 19 Jan 2023 22:21:30 +0000

backintime (1.3.3-1) unstable; urgency=medium

  [Jonathan Wiltshire]
  * Drop backintime-qt4 transitional package (Closes: #1010221)

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Set upstream metadata fields: Repository-Browse.
  * debian/watch: Use GitHub /tags rather than /releases page.
  * Update standards version to 4.6.1, no changes needed.
  * Update lintian override info format.
  * Remove constraints unnecessary since buster:
    + backintime-qt: Drop versioned constraint on backintime-gnome and
      backintime-kde in Replaces.
    + backintime-qt: Drop versioned constraint on backintime-gnome and
      backintime-kde in Breaks.

  [ Fabio Fantoni ]
  * Fix debian/watch.
  * New upstream version 1.3.3 (Closes: #1027823).

 -- Jonathan Wiltshire <jmw@debian.org>  Thu, 19 Jan 2023 21:47:52 +0000

backintime (1.3.2-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release (Closes: #1008653, #865744).
  * d/watch: support also latest versions without initial v
    (Closes: #1003776).
  * Remove d/patches: applied upstream.
  * Update d/copyright.

 -- Fabio Fantoni <fantonifabio@tiscali.it>  Mon, 25 Apr 2022 14:02:48 +0200

backintime (1.2.1-3) unstable; urgency=medium

  * Cherry-pick patch for #946349 from upstream Git repository
    (Closes: #946349).

 -- Fabian Wolff <fabi.wolff@arcor.de>  Mon, 31 May 2021 15:14:50 +0200

backintime (1.2.1-2) unstable; urgency=medium

  * Source-only reupload after the package has been in the NEW queue
    (due to backintime-qt).
  * Change incorrect dependency on python3-dbus.mainloop.qt to the Qt 5
    python3-dbus.mainloop.pyqt5 (thanks to Michael Weghorn for pointing
    this out to me!) (Closes: #941686).
  * Audit and update debian/copyright (Closes: #941984, #942155).
  * Upgrade to Standards-Version 4.4.1 (no changes).
  * Add additional backintime-qt_polkit.1.gz symlink to backintime.1.gz
    to silence the binary-without-manpage Lintian warning.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Wed, 30 Oct 2019 22:35:50 +0100

backintime (1.2.1-1) unstable; urgency=medium

  [ Fabian Wolff ]
  * Add myself as co-maintainer in debian/control.
  * New upstream release (Closes: #930589, #935341, #879611).
  * Upgrade to Standards-Version 4.4.0 (no changes).
  * Upgrade to debhelper compat level 12.
  * Remove debian/patches (all fixed upstream).
  * Introduce new backintime-qt package, because upstream moved from
    Qt 4 to Qt 5. Make backintime-qt4 a transitional package.
  * Drop the transitional backintime-{gnome,kde} packages
    (Closes: #939139, #939140).

  [ Jonathan Wiltshire ]
  * Team upload.
  * Add a debian/NEWS entry regarding possible change to repository
    permissions

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 15 Sep 2019 15:02:51 +0100

backintime (1.1.24-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release (Closes: #879609, #881205)
    (fixes CVE-2017-16667).
  * Update debian/watch to version 4 format (no changes).
  * Remove patches 01-858193-back-up-slash-root-perms.patch and
    02-polkit-vuln.patch (fixed upstream).
  * Set Priority to optional in debian/control (extra is deprecated).
  * Update Vcs-Git and Vcs-Browser fields in debian/control.
  * Delete trailing whitespace from debian/{changelog,control} in
    order to silence the file-contains-trailing-whitespace Lintian
    tag.
  * Upgrade to debhelper compat level 11.
  * Upgrade to Standards-Version 4.1.5 in debian/control.
  * Add patches 01-fix-spelling-errors.patch and
    02-fix-man-error.patch.
  * Remove unused desktop-command-not-in-package Lintian override.
  * Update debian/copyright.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Tue, 17 Jul 2018 13:02:53 +0200

backintime (1.1.12-2) unstable; urgency=high

  * 01-858193-back-up-slash-root-perms.patch: back up permissions
    of '/' as well (Closes: #858193)
  * 02-polkit-vuln.patch: fix race condition in polkit privilege
    authorisation (CVE-2017-7572) (Closes: #859815)
  * Build-depend on dh-python

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 22 Apr 2017 17:21:03 +0100

backintime (1.1.12-1) unstable; urgency=medium

  [ Germar Reitze ]
  * New upstream release
      Closes: #802890, #787959, #783386, #789258, #620244, #659965
      LP: #1391589, #1391767, #1117709, #1172114, #1317795, #1441672
  * debian/control: add virtual cron-daemon depend (Closes: #776856)
  * debian/control: depend on openssh-client (won't start without ssh-agent)
  * debian/control: new upstream project Homepage
  * debian/watch: new upstream hosting

  [ Jonathan Wiltshire ]
  * Fix transitional package breaks/replaces (Closes: #806545)
  * Remove patches/timestamps-in-gzip.patch, integrated upstream

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 07 Feb 2016 12:16:34 +0000

backintime (1.1.6-1) unstable; urgency=medium

  * New upstream release: remove kde and gnome front ends and replace with qt4
    (Closes: #786520, #775430, #735122, #790574, #780870)
  * Debhelper compat level 9
  * Install upstream changelog and other files so they show up in help/about
  * Add lintian override for backintime-qt4:desktop-command-not-in-package
  * Add lintian override for backintime-common:extra-license-file
  * debian/control: remove X-Python3-Version
  * Update copyright
  * timestamps-in-gzip.patch: Keep timestamps out of gzip files for
    reproducibility

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 23 Aug 2015 22:19:17 +0100

backintime (1.0.36-1) unstable; urgency=medium

  * Acknowledge NMU; thanks Julian
  * New upstream release
    (LP: #1340131, #1335545, #1276348, #1332126, #1316288, #1269991)
  * Install libnotify plugin in common package
  * debian/watch: consider only numeric versions
  * d/rules: add additional files to be removed in clean
  * Standards version 3.9.6 (no changes)
  * Update debian/copyright (Closes: #747212)
  * Add patch header to install-docs-move.patch
  * d/control: add additional suggested packages and tweak GNOME
    package description (Closes: #745122)

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 26 Oct 2014 12:10:12 +0000

backintime (1.0.34-0.1) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream release (Closes: #707198, #730341, #727213)
  * Remove portable-configure patch, as now applied upstream; extend
    desktop-su patch to include kde4
  * Moved documentation from /usr/share/doc/backintime to
    /usr/share/doc/backintime-common, as there is no "backintime" binary
    package
  * Updated debian/watch to match new repository filenames
  * Might also close some of the other outstanding bugs; these need to be
    triaged

 -- Julian Gilbey <jdg@debian.org>  Thu, 02 Jan 2014 10:03:47 +0000

backintime (1.0.10-1) unstable; urgency=low

  * New upstream release (Closes: #656855, #642050)
  * Patch kde-html-dir.patch merged upstream
  * Standards version 3.9.3 (no changes required)

 -- Jonathan Wiltshire <jmw@debian.org>  Sun, 20 May 2012 22:17:54 +0100

backintime (1.0.8-1) unstable; urgency=low

  * Imported Upstream version 1.0.8
  * Patch no-empty-figures.patch is applied upstream
  * Install KDE documentation to /usr/share/doc/kde/* (Closes: #628429)
    (LP: #806138)
  * Standards version 3.9.2 (no changes)
  * Migrate from python-support to dh_python2

 -- Jonathan Wiltshire <jmw@debian.org>  Tue, 05 Jul 2011 21:32:52 +0100

backintime (1.0.6-1) unstable; urgency=low

  * New upstream release (LP: #675696)
  * Don't create empty directories during build (patch no-empty-
    figures.patch) (LP: #696663)
  * Upload to unstable

 -- Jonathan Wiltshire <jmw@debian.org>  Mon, 03 Jan 2011 01:19:10 +0000

backintime (1.0.4-1) experimental; urgency=low

  * New upstream release
      Closes: #555293
      LP: #409130 #507246 #528518 #534829 #522618
  * Update debian/copyright
  * The following patches are either integrated or fixed properly
    upstream: no-chmod-777.patch allow-root-backup.patch
  * Refactor remaining patches and make the headers DEP-3 compliant
  * Convert to source format 3.0 (quilt) and drop quilt dependency and
    logic
  * Don't depend on a specific version of Python; let dh_python choose
    the best option
  * Remove the "earlier-than" restriction for the Conflicts on
    backintime-kde4
  * Standards version 3.9.1 (no changes required)
  * Update my email address

 -- Jonathan Wiltshire <jmw@debian.org>  Fri, 03 Dec 2010 21:56:53 +0000

backintime (0.9.26-4) unstable; urgency=low

  * debian/control: depend on the menu packages for both GNOME and KDE
    front-ends, so we can use su-to-root on headless systems (LP:
    #466088)
  * Patch allow-root-backup.patch: allow backup of root directory,
    cherry-picked from upstream (LP: #588841) - thanks to jerico
  * Explicitly declare source format 1.0
  * Standards version 3.9.0 (no changes)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Mon, 12 Jul 2010 00:18:27 +0100

backintime (0.9.26-3) unstable; urgency=high

  * Fix typo in debian/rules
  * New patch no-chmod-777.patch to stop common/snapshots.py from making
    all files world-readable and writeable before deleting a backup.
    (Closes: #543785) - thanks to Rémi Vanicat, Bart de Koning

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Mon, 07 Sep 2009 21:53:28 +0100

backintime (0.9.26-2) unstable; urgency=medium

  * Standards version 3.8.3 (no changes required)
  * Use printf to make common/configure portable (Closes: #543053)
  * Convert all Debian modifications into Quilt patches
  * Recommend meld for backintime-gnome (Closes: #542861)
  * Remove transitional package backintime-kde4
  * Call dh_clean last in the clean sequence (Closes: #543168)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sun, 23 Aug 2009 21:41:30 +0100

backintime (0.9.26-1) unstable; urgency=low

  * New upstream version:
    - updated translations
    - allow backup of a destination's parent folder
    - improvements to the interfaces
    - expert option for changing the niceness of automatic backups
  * Fix inter-dependencies for the transitional package backintime-kde4
    (Closes: #529400) - thanks to JJ Luza
  * Front-end checks during builds are disabled properly, not as a
    direct source change

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Tue, 19 May 2009 19:18:28 +0100

backintime (0.9.24-2) unstable; urgency=low

  * Rename binary package backintime-kde4 to backintime-kde (and handle
    upgrades for users when it reaches the archive)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Mon, 18 May 2009 20:12:34 +0100

backintime (0.9.24-1) unstable; urgency=low

  * New upstream version (closes: #527447):
    - backintime is no longer aware of 'backintime-gnome' and 'backintime-kde4'
      (you need run 'backintime-gnome' for GNOME version and 'backintime-kde4'
      for KDE4 version)
    - fix a bug that crashes the program after taking a snapshot
  * Update homepage field in debian/control (closes: #527595)
  * Refactor packaging to fit new upstream build system (an almost entire
    re-write of debian/rules)
  * Make configure scripts use /bin/sh instead of /bin/bash (they don't use
    bash features)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sat, 16 May 2009 23:04:32 +0100

backintime (0.9.22.1-1) unstable; urgency=low

  * New upstream version:
     - follow FreeDesktop directory specs
     - treat "*~" files as hidden
     - new user callback system
  * Refactor debian/rules to fit new upstream build system
  * Remove the tests for GNOME and KDE4 in ./configure (on an
    autobuilder, they will always fail)
  * Bump debhelper build-dependency to 7.2.8

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Sun, 03 May 2009 23:02:46 +0100

backintime (0.9.20-1) unstable; urgency=low

  * New upstream version
  * Upload to unstable

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Wed, 08 Apr 2009 16:47:28 +0100

backintime (0.9.18-1) experimental; urgency=low

  * New upstream version
  * Make the new "run as root" desktop entries use su-to-root
    Add "run as root" entry to Debian menu system

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Fri, 03 Apr 2009 12:05:08 +0100

backintime (0.9.16.1-1) experimental; urgency=low

  * New upstream version
  * Add versioned depends on the -common package
  * Added dependency on python-gnome2 for -gnome
  * Added dependency on python-notify for -gnome
  * Bump standards version
  * Make cron a Depends instead of a Recommends

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Tue, 17 Mar 2009 01:54:40 +0000

backintime (0.9.4-1) experimental; urgency=low

  * Initial release (Closes: #508407)

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Fri, 16 Jan 2009 15:34:42 +0000
